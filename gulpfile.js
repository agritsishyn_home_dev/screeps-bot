let browserify = require('browserify');
let gulp = require('gulp');
let source = require('vinyl-source-stream');
let gutil = require('gulp-util');
let tsify = require('tsify');
let screeps = require('gulp-screeps');
var fs = require('fs');

gulp.task('default', done => {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['./src/main.ts'],
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .bundle()
        .pipe(source('main.js'))
        .on('error', gutil.log)
        .pipe(gulp.dest("./build/"));
});

gulp.task('deploy', ['default'], done => {
    let conf = JSON.parse(fs.readFileSync('./screeps_login.json', 'utf8'));

    return gulp.src('./build/main.js')
        .pipe(screeps(conf));
});